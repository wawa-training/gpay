CREATE TABLE IF NOT EXISTS `gpay_db`.`tbl_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount_transferred` double DEFAULT NULL,
  `to_phone_number` varchar(45) DEFAULT NULL,
  `transaction_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `transaction_message` varchar(255) DEFAULT NULL,
  `from_phone_number` varchar(45) DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT 'SUCCESS',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `gpay_db`.`tbl_user` (
  `user_aid` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NULL,
  `last_name` VARCHAR(100) NULL,
  `phone_number` VARCHAR(45) NOT NULL,
  `age` VARCHAR(45) NULL,
  `created_date` DATETIME NULL,
  `updated_date` DATETIME NULL,
  `status` VARCHAR(45) NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`user_aid`),
  UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC))
ENGINE = InnoDB