package com.gpay.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gpay.dto.MessageResponse;
import com.gpay.dto.StatementResponseDto;
import com.gpay.dto.UserRequestDto;
import com.gpay.dto.WalletTransferRequestDto;
import com.gpay.service.GpayService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/wallet")
public class GpayController {

	@Autowired
	private GpayService gpayService;

	@ApiOperation(value = "User registration")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "user is registered"),
			@ApiResponse(code = 302, message = "user already registered", response = MessageResponse.class) ,
			@ApiResponse(code = 500, message = "Internal Error", response = MessageResponse.class) })
	@PostMapping("/register")
	public ResponseEntity<Object> register(@RequestBody @Valid UserRequestDto userRequestDto) {
		return gpayService.register(userRequestDto);
	}

	@ApiOperation(value = "Fund Transfer")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully Transfered", response = MessageResponse.class),
			@ApiResponse(code = 500, message = "Internal Error", response = MessageResponse.class) })
	@PostMapping("/payment")
	public ResponseEntity<Object> payment(@RequestBody WalletTransferRequestDto walletTransferRequestDto) {
		return gpayService.payment(walletTransferRequestDto);
	}

	
	@ApiResponses(value = {
			@ApiResponse(code = 302, message = "Statement Found", response = StatementResponseDto.class),
			@ApiResponse(code = 204, message = "Statement Not Found", response = MessageResponse.class),
			@ApiResponse(code = 500, message = "Internal Error", response = MessageResponse.class) })
	@GetMapping("/statement/{phoneNumber}")
	public ResponseEntity<Object> getStatements(@PathVariable String phoneNumber,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
		return gpayService.getStatements(phoneNumber, page, size);
	}
}
