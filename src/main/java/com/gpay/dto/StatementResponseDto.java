package com.gpay.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatementResponseDto {
	
	

	public StatementResponseDto(List<StatementDto> statementDtos, int currentStatementPageTransaction,
			long totalTransactions, int totalStatementPage) {
		super();
		this.statementDtos = statementDtos;
		this.currentStatementPageTransaction = currentStatementPageTransaction;
		this.totalTransactions = totalTransactions;
		this.totalStatementPage = totalStatementPage;
	}
	private List<StatementDto> statementDtos;
	private int currentStatementPageTransaction;
	private long totalTransactions;
	private int totalStatementPage;
	public List<StatementDto> getStatementDtos() {
		return statementDtos;
	}
	public void setStatementDtos(List<StatementDto> statementDtos) {
		this.statementDtos = statementDtos;
	}
	public int getCurrentStatementPageTransaction() {
		return currentStatementPageTransaction;
	}
	public void setCurrentStatementPageTransaction(int currentStatementPageTransaction) {
		this.currentStatementPageTransaction = currentStatementPageTransaction;
	}
	public long getTotalTransactions() {
		return totalTransactions;
	}
	public void setTotalTransactions(long totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	public int getTotalStatementPage() {
		return totalStatementPage;
	}
	public void setTotalStatementPage(int totalStatementPage) {
		this.totalStatementPage = totalStatementPage;
	}
	
	
}
