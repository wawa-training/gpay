package com.gpay.dto;

import java.sql.Timestamp;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatementDto {

	
	
	public StatementDto() {
		super();
	}
	public StatementDto(int transactionId, double amountTransferred, String toPhoneNumber, Timestamp transactionDate,
			String transactionMessage, String fromPhoneNumber, String status) {
		super();
		this.transactionId = transactionId;
		this.amountTransferred = amountTransferred;
		this.toPhoneNumber = toPhoneNumber;
		this.transactionDate = transactionDate;
		this.transactionMessage = transactionMessage;
		this.fromPhoneNumber = fromPhoneNumber;
		this.status = status;
	}
	private int transactionId;
	private double amountTransferred;
	private String toPhoneNumber;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Timestamp transactionDate;
	private String transactionMessage;
	private String fromPhoneNumber;
	private String status;
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public double getAmountTransferred() {
		return amountTransferred;
	}
	public void setAmountTransferred(double amountTransferred) {
		this.amountTransferred = amountTransferred;
	}
	public String getToPhoneNumber() {
		return toPhoneNumber;
	}
	public void setToPhoneNumber(String toPhoneNumber) {
		this.toPhoneNumber = toPhoneNumber;
	}
	public Timestamp getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getTransactionMessage() {
		return transactionMessage;
	}
	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}
	public String getFromPhoneNumber() {
		return fromPhoneNumber;
	}
	public void setFromPhoneNumber(String fromPhoneNumber) {
		this.fromPhoneNumber = fromPhoneNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
}
