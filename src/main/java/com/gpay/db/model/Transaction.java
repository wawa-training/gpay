// Generated with g9.

package com.gpay.db.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="tbl_transaction")
public class Transaction implements Serializable {

    /** Primary key. */
    protected static final String PK = "transactionId";

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="transaction_id", unique=true, nullable=false, precision=10)
    private int transactionId;
    @Column(name="amount_transferred", length=22)
    private double amountTransferred;
    @Column(name="to_phone_number", length=45)
    private String toPhoneNumber;
    @Column(name="transaction_date")
    private Timestamp transactionDate;
    @Column(name="transaction_message", length=255)
    private String transactionMessage;
    @Column(name="from_phone_number", length=45)
    private String fromPhoneNumber;
    @Column(length=45)
    private String status;

    /** Default constructor. */
    public Transaction() {
        super();
    }

    /**
     * Access method for transactionId.
     *
     * @return the current value of transactionId
     */
    public int getTransactionId() {
        return transactionId;
    }

    /**
     * Setter method for transactionId.
     *
     * @param aTransactionId the new value for transactionId
     */
    public void setTransactionId(int aTransactionId) {
        transactionId = aTransactionId;
    }

    /**
     * Access method for amountTransferred.
     *
     * @return the current value of amountTransferred
     */
    public double getAmountTransferred() {
        return amountTransferred;
    }

    /**
     * Setter method for amountTransferred.
     *
     * @param aAmountTransferred the new value for amountTransferred
     */
    public void setAmountTransferred(double aAmountTransferred) {
        amountTransferred = aAmountTransferred;
    }

    /**
     * Access method for toPhoneNumber.
     *
     * @return the current value of toPhoneNumber
     */
    public String getToPhoneNumber() {
        return toPhoneNumber;
    }

    /**
     * Setter method for toPhoneNumber.
     *
     * @param aToPhoneNumber the new value for toPhoneNumber
     */
    public void setToPhoneNumber(String aToPhoneNumber) {
        toPhoneNumber = aToPhoneNumber;
    }

    /**
     * Access method for transactionDate.
     *
     * @return the current value of transactionDate
     */
    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    /**
     * Setter method for transactionDate.
     *
     * @param aTransactionDate the new value for transactionDate
     */
    public void setTransactionDate(Timestamp aTransactionDate) {
        transactionDate = aTransactionDate;
    }

    /**
     * Access method for transactionMessage.
     *
     * @return the current value of transactionMessage
     */
    public String getTransactionMessage() {
        return transactionMessage;
    }

    /**
     * Setter method for transactionMessage.
     *
     * @param aTransactionMessage the new value for transactionMessage
     */
    public void setTransactionMessage(String aTransactionMessage) {
        transactionMessage = aTransactionMessage;
    }

    /**
     * Access method for fromPhoneNumber.
     *
     * @return the current value of fromPhoneNumber
     */
    public String getFromPhoneNumber() {
        return fromPhoneNumber;
    }

    /**
     * Setter method for fromPhoneNumber.
     *
     * @param aFromPhoneNumber the new value for fromPhoneNumber
     */
    public void setFromPhoneNumber(String aFromPhoneNumber) {
        fromPhoneNumber = aFromPhoneNumber;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Compares the key for this instance with another TblTransaction.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class TblTransaction and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Transaction)) {
            return false;
        }
        Transaction that = (Transaction) other;
        if (this.getTransactionId() != that.getTransactionId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another TblTransaction.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Transaction)) return false;
        return this.equalKeys(other) && ((Transaction)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getTransactionId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[TblTransaction |");
        sb.append(" transactionId=").append(getTransactionId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("transactionId", Integer.valueOf(getTransactionId()));
        return ret;
    }

}
