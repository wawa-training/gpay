// Generated with g9.

package com.gpay.db.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="tbl_user", indexes={@Index(name="tbl_user_phone_number_IX", columnList="phone_number", unique=true)})
public class User implements Serializable {

    /** Primary key. */
    protected static final String PK = "userAid";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="user_aid", unique=true, nullable=false, precision=10)
    private int userAid;
    @Column(name="first_name", length=100)
    private String firstName;
    @Column(name="last_name", length=100)
    private String lastName;
    @Column(name="phone_number", unique=true, nullable=false, length=45)
    private String phoneNumber;
    @Column(length=45)
    private String age;
    @Column(name="created_date")
    private Timestamp createdDate;
    @Column(name="updated_date")
    private Timestamp updatedDate;
    @Column(length=45)
    private String status;

    /** Default constructor. */
    public User() {
        super();
    }

    /**
     * Access method for userAid.
     *
     * @return the current value of userAid
     */
    public int getUserAid() {
        return userAid;
    }

    /**
     * Setter method for userAid.
     *
     * @param aUserAid the new value for userAid
     */
    public void setUserAid(int aUserAid) {
        userAid = aUserAid;
    }

    /**
     * Access method for firstName.
     *
     * @return the current value of firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method for firstName.
     *
     * @param aFirstName the new value for firstName
     */
    public void setFirstName(String aFirstName) {
        firstName = aFirstName;
    }

    /**
     * Access method for lastName.
     *
     * @return the current value of lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method for lastName.
     *
     * @param aLastName the new value for lastName
     */
    public void setLastName(String aLastName) {
        lastName = aLastName;
    }

    /**
     * Access method for phoneNumber.
     *
     * @return the current value of phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Setter method for phoneNumber.
     *
     * @param aPhoneNumber the new value for phoneNumber
     */
    public void setPhoneNumber(String aPhoneNumber) {
        phoneNumber = aPhoneNumber;
    }

    /**
     * Access method for age.
     *
     * @return the current value of age
     */
    public String getAge() {
        return age;
    }

    /**
     * Setter method for age.
     *
     * @param aAge the new value for age
     */
    public void setAge(String aAge) {
        age = aAge;
    }

    /**
     * Access method for createdDate.
     *
     * @return the current value of createdDate
     */
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    /**
     * Setter method for createdDate.
     *
     * @param aCreatedDate the new value for createdDate
     */
    public void setCreatedDate(Timestamp aCreatedDate) {
        createdDate = aCreatedDate;
    }

    /**
     * Access method for updatedDate.
     *
     * @return the current value of updatedDate
     */
    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Setter method for updatedDate.
     *
     * @param aUpdatedDate the new value for updatedDate
     */
    public void setUpdatedDate(Timestamp aUpdatedDate) {
        updatedDate = aUpdatedDate;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Compares the key for this instance with another TblUser.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class TblUser and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof User)) {
            return false;
        }
        User that = (User) other;
        if (this.getUserAid() != that.getUserAid()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another TblUser.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof User)) return false;
        return this.equalKeys(other) && ((User)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getUserAid();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[TblUser |");
        sb.append(" userAid=").append(getUserAid());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("userAid", Integer.valueOf(getUserAid()));
        return ret;
    }

}
