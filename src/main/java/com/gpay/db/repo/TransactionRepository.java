package com.gpay.db.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gpay.db.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer>{

	List<Transaction> findByFromPhoneNumber(String fromPhoneNumber);
	List<Transaction> findByFromPhoneNumberAndStatus(String fromPhoneNumber,String status);
	Page<Transaction> findByFromPhoneNumberOrToPhoneNumber(String fromPhoneNumber,String toPhoneNumber,Pageable pageable);
}
