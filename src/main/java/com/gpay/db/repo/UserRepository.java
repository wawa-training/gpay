package com.gpay.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gpay.db.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByPhoneNumber(String phoneNumber);
	User findByPhoneNumberAndStatus(String phoneNumber,String status);
	
}
