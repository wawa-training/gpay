package com.gpay.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.gpay.dto.WalletPaymentResponse;
import com.gpay.dto.WalletTransferRequestDto;


@FeignClient(name="http://wawa.bank/bank-service/wallet/account")
public interface WalletServiceClient {
	
	@GetMapping("/{phoneNumber}")
	public String validate(@PathVariable String phoneNumber);
	
	@PostMapping("/payment")
	public WalletPaymentResponse walletPayment(@RequestBody WalletTransferRequestDto walletTransferRequestDto);

}
