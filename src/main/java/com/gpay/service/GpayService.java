package com.gpay.service;


import org.springframework.http.ResponseEntity;

import com.gpay.dto.UserRequestDto;
import com.gpay.dto.WalletTransferRequestDto;

public interface GpayService {

	ResponseEntity<Object> register(UserRequestDto requestDto);

	ResponseEntity<Object> payment(WalletTransferRequestDto walletTransferRequestDto);

	ResponseEntity<Object> getStatements(String phoneNumber, int page, int size);
}
