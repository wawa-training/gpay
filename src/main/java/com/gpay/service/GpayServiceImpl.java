package com.gpay.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import static org.springframework.http.HttpStatus.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.gpay.client.WalletServiceClient;
import com.gpay.db.model.Transaction;
import com.gpay.db.model.User;
import com.gpay.db.repo.TransactionRepository;
import com.gpay.db.repo.UserRepository;
import com.gpay.dto.MessageResponse;
import com.gpay.dto.StatementDto;
import com.gpay.dto.StatementResponseDto;
import com.gpay.dto.Status;
import com.gpay.dto.UserRequestDto;
import com.gpay.dto.WalletPaymentResponse;
import com.gpay.dto.WalletTransferRequestDto;

@Service
@Transactional
public class GpayServiceImpl implements GpayService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private WalletServiceClient walletServiceClient;

	@Override
	public ResponseEntity<Object> register(UserRequestDto requestDto) {
		ResponseEntity<Object> response = null;
		LocalDateTime now = LocalDateTime.now();
		Timestamp currentTime = Timestamp.valueOf(now);
		try {
			String status = walletServiceClient.validate(requestDto.getPhoneNumber());
			if (!status.equalsIgnoreCase("VALID")) {
				response = new ResponseEntity<>(
						new MessageResponse(requestDto.getPhoneNumber() + " is not valid for any Account"),
						INTERNAL_SERVER_ERROR);

			} else {
				if (userRepository.findByPhoneNumber(requestDto.getPhoneNumber())!=null) {
					
					return new ResponseEntity<>(new MessageResponse("User already registered"), FOUND);
				}
				User user = new User();
				BeanUtils.copyProperties(requestDto, user);
				user.setCreatedDate(currentTime);
				user.setUpdatedDate(currentTime);
				user.setStatus(Status.ACTIVE.toString());
				userRepository.save(user);
				response = new ResponseEntity<>(new MessageResponse("User registered Successfully"), CREATED);
			}

		} catch (Exception e) {
			response = new ResponseEntity<>(new MessageResponse(e.getMessage()), INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<Object> payment(WalletTransferRequestDto walletTransferRequestDto) {
		ResponseEntity<Object> response = null;
		LocalDateTime now = LocalDateTime.now();
		Timestamp currentTime = Timestamp.valueOf(now);

		try {
			Transaction transaction = new Transaction();
			BeanUtils.copyProperties(walletTransferRequestDto, transaction);
			transaction.setTransactionDate(currentTime);
			WalletPaymentResponse paymentResponse = walletServiceClient.walletPayment(walletTransferRequestDto);

			if (paymentResponse.getStatus() == Status.SUCCESS) {
				transaction.setStatus(Status.SUCCESS.toString());
				transaction.setTransactionMessage("Successfully Transfered");
				transactionRepository.save(transaction);
				response = new ResponseEntity<>(new MessageResponse("Successfully Transfered"), OK);
			} else {
				transaction.setStatus(Status.FAIL.toString());
				transaction.setTransactionMessage(paymentResponse.getMessage());
				transactionRepository.save(transaction);
				response = new ResponseEntity<>(
						new MessageResponse("Transaction status:" + paymentResponse.getMessage()),
						INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			response = new ResponseEntity<>(new MessageResponse(e.getMessage()), INTERNAL_SERVER_ERROR);

		}
		return response;
	}

	@Override
	public ResponseEntity<Object> getStatements(String phoneNumber, int page, int size) {
		ResponseEntity<Object> response = null;
		List<Transaction> transactions = new ArrayList<>();

		try {

			Pageable pageable = PageRequest.of(page, size);
			Page<Transaction> transactionPage = transactionRepository.findByFromPhoneNumberOrToPhoneNumber(phoneNumber, phoneNumber, pageable);
			transactions = transactionPage.getContent();
			if (transactions.isEmpty()) {
				return new ResponseEntity<>(NO_CONTENT);
			}

			List<StatementDto> dtos = new ArrayList<>();
			StatementDto dto = null;
			for (Transaction transaction : transactions) {
				dto = new StatementDto();
				BeanUtils.copyProperties(transaction, dto);
				dtos.add(dto);
			}
			System.out.println("transactionPage.getNumber()"+transactionPage.getNumber());
			StatementResponseDto statementResponseDto = new StatementResponseDto(dtos, dtos.size(),
					transactionPage.getTotalElements(), transactionPage.getTotalPages());
			response = new ResponseEntity<Object>(statementResponseDto, FOUND);
		} catch (Exception e) {
			response = new ResponseEntity<>(new MessageResponse(e.getMessage()), INTERNAL_SERVER_ERROR);

		}
		return response;
	}

}
